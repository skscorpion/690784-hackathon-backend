﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Outreach.Reporting.Business.Interfaces;
using Outreach.Reporting.Business.Processors;
using Outreach.Reporting.Data.Interfaces;
using Outreach.Reporting.Entity.Entities;
using Outreach.Reporting.Service.Controllers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Xunit;
namespace UnitTest.Controllers
{
    public class EnrollmentControllerTest
    {
        private readonly IEnumerable<Enrollment> _entrollments;
        private readonly Mock<IEnrollmentProcessor> _entrollmentProcessorMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Enrollment _entrollment;
        private readonly ILogger<EnrollmentController> _loggerMock;
        private readonly IHostingEnvironment _hostingEnvironment;
        public EnrollmentControllerTest()
        {
           // _loggerMock = new ILogger<EnrollmentController>();
           // _logger = _logger.Object;
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _entrollments = new List<Enrollment>
            {
                 new Enrollment(),
                 new Enrollment()
            };
        }
        [Fact]
        public void TestGetEnrolledAssociates_IsNotNull()
        {
            //var mockId = It.IsAny<int?>();
            _unitOfWorkMock.Setup(repo => repo.Enrollments.GetEnrolledAssociates()).Returns(_entrollments);
            var processor = new EnrollmentProcessor(_unitOfWorkMock.Object);
            var controller = new EnrollmentController(processor);

            //Act
            var response = controller.GetEnrolledAssociates();

            //Assert
            Assert.NotNull(response);
        }
        [Fact]
        public void TestGetEnrolledAssociates_IsNull()
        {
            //Arrange
            //var mockId = It.IsAny<int?>();
            _unitOfWorkMock.Setup(repo => repo.Enrollments.GetEnrolledAssociates()).Throws(null);
            var processor = new EnrollmentProcessor(_unitOfWorkMock.Object);
            var controller = new EnrollmentController(processor);

            //Act
            var response = new ContentResult { StatusCode = (int)HttpStatusCode.NoContent };

            //Assert
            var contentResult = Assert.IsType<ContentResult>(response);
            Assert.Null(contentResult.Content);
        }
        [Fact]
        public void TestGetEnrolledAssociates_BadRequest()
        {
            //Arrange
            //var mockId = It.IsAny<int?>();
            _unitOfWorkMock.Setup(repo => repo.Enrollments.GetEnrolledAssociates()).Throws(new NullReferenceException());
            var processor = new EnrollmentProcessor(_unitOfWorkMock.Object);
            var controller = new EnrollmentController(processor);

            //Act
            var response = controller.GetEnrolledAssociates();

            //Assert
            var badRequestResult = Assert.IsType<BadRequestResult>(response);
            Assert.Equal(400, badRequestResult.StatusCode);
        }
       // [Fact]
        //public void TestGetEnrolledUniqueAssociates_IsNotNull()
        //{
        //    //var mockId = It.IsAny<int?>();
        //    _unitOfWorkMock.Setup(repo => repo.Enrollments.).Returns(_entrollments);
        //    var processor = new EnrollmentProcessor(_unitOfWorkMock.Object);
        //    var controller = new EnrollmentController(processor, _loggerMock, _hostingEnvironment);

        //    //Act
        //    var response = controller.GetEnrolledUniqueAssociates();

        //    //Assert
        //    Assert.NotNull(response);
        //}
    }
}
