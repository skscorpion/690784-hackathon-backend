﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Outreach.Reporting.Data.Migrations
{
    public partial class userrole_table_description_field_adde : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "ApplicationUsers",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "AssociateID",
                table: "ApplicationUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 547, DateTimeKind.Local).AddTicks(4315));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 549, DateTimeKind.Local).AddTicks(4130));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 549, DateTimeKind.Local).AddTicks(4158));

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUsers_AssociateID",
                table: "ApplicationUsers",
                column: "AssociateID");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUsers_Associates_AssociateID",
                table: "ApplicationUsers",
                column: "AssociateID",
                principalTable: "Associates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUsers_Associates_AssociateID",
                table: "ApplicationUsers");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUsers_AssociateID",
                table: "ApplicationUsers");

            migrationBuilder.DropColumn(
                name: "AssociateID",
                table: "ApplicationUsers");

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "ApplicationUsers",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 16, 56, 44, 145, DateTimeKind.Local).AddTicks(647));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 16, 56, 44, 146, DateTimeKind.Local).AddTicks(4932));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 16, 56, 44, 146, DateTimeKind.Local).AddTicks(4951));
        }
    }
}
