﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Outreach.Reporting.Data.Migrations
{
    public partial class event_date_type_Changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "Events",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "Events",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
