﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Outreach.Reporting.Data.Migrations
{
    public partial class userrole_table_description_field_ad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 7, 56, 851, DateTimeKind.Local).AddTicks(1985));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 7, 56, 852, DateTimeKind.Local).AddTicks(9281));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 7, 56, 852, DateTimeKind.Local).AddTicks(9307));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 547, DateTimeKind.Local).AddTicks(4315));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 549, DateTimeKind.Local).AddTicks(4130));

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "ID",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 3, 12, 17, 0, 23, 549, DateTimeKind.Local).AddTicks(4158));
        }
    }
}
