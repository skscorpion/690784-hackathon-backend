﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Outreach_Reporting_System.Helpers
{
    public class AppSettings
    {
        public string DatabaseConnectionString { get; set; }
    }
}
