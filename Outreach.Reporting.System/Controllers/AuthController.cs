﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Outreach.Reporting.Business.Interfaces;
using Outreach.Reporting.Entity.Entities;

namespace Outreach.Reporting.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthProcessor _authProcessor;
        public AuthController(IAuthProcessor authProcessor)
        {
            _authProcessor = authProcessor;
        }

        // POST api/GetToken/{email_id}
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody]int userId)
        {
            if (userId == 0)
                return BadRequest();

            if (_authProcessor.AuthenticateUser(userId) || _authProcessor.CheckPocById(userId))
            {
                string userRole = _authProcessor.GetUserRoleById(userId);
                if (string.IsNullOrEmpty(userRole))
                    userRole = "POC";//if authenticated user role is null then set POC role

                IActionResult response = Unauthorized();
               
                    var tokenString = GenerateJSONWebToken(userId, userRole);

                return await Task.FromResult(Ok(new { token = tokenString, role = userRole }));
            }
            else
                return Unauthorized();
        }
        private string GenerateJSONWebToken(int userId, string userRole)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this_is_our_super_long_security_key_for_outreach_reporting_system_project_2019_03_10$"));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Role, userRole)); 
            claims.Add(new Claim("UserId", userId.ToString()));
            
            var token = new JwtSecurityToken(
                issuer: "outreachReportingSystem",
              audience: "reportUsers",
              claims: claims,
              expires: DateTime.Now.AddMinutes(60),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}